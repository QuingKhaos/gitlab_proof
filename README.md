# OpenPGP GitLab proof

This is an OpenPGP proof that connects [my OpenPGP key](https://keyoxide.org/E81B098BEE0606737E23CD6E7EDBFC7C859B32A4) to [this GitLab account](https://gitlab.com/QuingKhaos). For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:E81B098BEE0606737E23CD6E7EDBFC7C859B32A4]
